import pytest
from bot import BookmarkRepository, Bookmark
from schema import SchemaError


def media_json():
    media_json = """[
    {
        "name": "test1",
        "content": {
            "body": "sdf",
            "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
            },
            "msgtype": "m.image",
            "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
        }
    },
    {
        "name": "test2",
        "content": {
            "body": "sdf",
            "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
        },
        "msgtype": "m.image",
        "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
        }
    },
    {
        "name": "test3",
        "content": {
            "body": "test3",
            "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/AAAAAAAAAAAAAAAAAAAAAAAA"
        },
        "msgtype": "m.image",
        "url": "mxc://server.org/BBBBBBBBBBBBBBBBBBBBBBBB"
        }
    }

]"""
    return media_json


@pytest.fixture
def create_repo(tmp_path):
    json_test = tmp_path / "media_test.json"
    print(json_test)
    json_test.write_text(media_json())
    return json_test


def test_json_dump(tmp_path):
    json_test = tmp_path / "test.json"
    repo = BookmarkRepository(json_test)
    media = Bookmark(
        "test1",
        """{
            "body": "sdf",
            "info": {
                "size": 48030,
                "mimetype": "image",
                "thumbnail_info": {
                    "w": 794,
                    "h": 414,
                    "mimetype": "image/png",
                    "size": 57054
                },
                "w": 794,
                "h": 414,
                "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
            },
            "msgtype": "m.image",
            "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
        }""",
    )
    media2 = Bookmark(
        "test2",
        """{
            "body": "sdf",
            "info": {
                "size": 48030,
                "mimetype": "image",
                "thumbnail_info": {
                    "w": 794,
                    "h": 414,
                    "mimetype": "image/png",
                    "size": 57054
                },
                "w": 794,
                "h": 414,
                "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
            },
            "msgtype": "m.image",
            "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
        }""",
    )
    media3 = Bookmark(
        "test3",
        """{
            "body": "test3",
            "info": {
                "size": 48030,
                "mimetype": "image",
                "thumbnail_info": {
                    "w": 794,
                    "h": 414,
                    "mimetype": "image/png",
                    "size": 57054
                },
                "w": 794,
                "h": 414,
                "thumbnail_url": "mxc://server.org/AAAAAAAAAAAAAAAAAAAAAAAA"
            },
            "msgtype": "m.image",
            "url": "mxc://server.org/BBBBBBBBBBBBBBBBBBBBBBBB"
        }""",
    )

    repo.add(media)
    repo.add(media2)
    repo.add(media3)
    assert json_test.read_text() == media_json()


def test_add_existing_name(create_repo):
    repo = BookmarkRepository(create_repo)
    content = """{
        "body": "something",
        "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
        },
        "msgtype": "m.image",
        "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
    }"""
    media = Bookmark("test1", content)
    with pytest.raises(ValueError):
        repo.add(media)

def test_empty_name(create_repo):
    repo = BookmarkRepository(create_repo)
    content = """{
        "body": "something",
        "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
        },
        "msgtype": "m.image",
        "url": "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"
    }"""
    with pytest.raises(SchemaError):
        Bookmark("", content)


def test_wrong_mxc():
    content = """{
        "body": "something",
        "info": {
            "size": 48030,
            "mimetype": "image",
            "thumbnail_info": {
                "w": 794,
                "h": 414,
                "mimetype": "image/png",
                "size": 57054
            },
            "w": 794,
            "h": 414,
            "thumbnail_url": "mxc://server.org/krkuwjNQFUncCijPhXyZZdOG"
        },
        "msgtype": "m.image",
        "url": "mxc://something.org/gnroALFYQyChjDdo"
    }"""
    with pytest.raises(SchemaError):
        Bookmark("test", content)


def test_get_bookmark(create_repo):
    repo = BookmarkRepository(create_repo)
    bookmark = repo.get("test1")
    assert bookmark.name == "test1"
    assert bookmark.content["url"] == "mxc://server.org/gnroEyRlKOmPALFYQyChjDdo"


def test_remove_bookmark(create_repo):
    repo = BookmarkRepository(create_repo)
    repo.remove("test1")
    assert len(repo.list()) == 2
